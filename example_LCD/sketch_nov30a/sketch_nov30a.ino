#include <Wire.h>
#include "LiquidCrystal_I2C.h"
LiquidCrystal_I2C lcd(0x27, 16, 2); // 0x27  มีสอง address 0x3F
void setup()
{
  lcd.begin();
  lcd.backlight();
  lcd.setCursor(1, 0); // กำหนดให้ เคอร์เซอร์ อยู่ตัวอักษรตำแหน่งที่0 แถวที่ 1 เตรียมพิมพ์ข้อความ
  lcd.print("I Love Arduino"); //พิมพ์ข้อความ "LCD1602 I2c Test"
  lcd.setCursor(1, 1); // กำหนดให้ เคอร์เซอร์ อยู่ตัวอักษรกำแหน่งที3 แถวที่ 2 เตรียมพิมพ์ข้อความ
  lcd.print("@asiamediasoft");

}
void loop() {
  delay(1000);
  lcd.setCursor(1, 0); // กำหนดให้ เคอร์เซอร์ อยู่ตัวอักษรตำแหน่งที่0 แถวที่ 1 เตรียมพิมพ์ข้อความ
  lcd.print("              "); //พิมพ์ข้อความ "LCD1602 I2c Test"
  lcd.setCursor(1, 1); // กำหนดให้ เคอร์เซอร์ อยู่ตัวอักษรกำแหน่งที3 แถวที่ 2 เตรียมพิมพ์ข้อความ
  lcd.print("              ");
  lcd.setCursor(1, 0); // กำหนดให้ เคอร์เซอร์ อยู่ตัวอักษรตำแหน่งที่0 แถวที่ 1 เตรียมพิมพ์ข้อความ
   delay(1000);

  lcd.print("I Love Arduino"); //พิมพ์ข้อความ "LCD1602 I2c Test"
  lcd.setCursor(1, 1); // กำหนดให้ เคอร์เซอร์ อยู่ตัวอักษรกำแหน่งที3 แถวที่ 2 เตรียมพิมพ์ข้อความ
  lcd.print("@asiamediasoft");
  lcd.backlight();

}