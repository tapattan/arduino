// C++ code
//

int buttonState = 0;
void setup()
{
  Serial.begin(9600);
  pinMode(2, INPUT);
  pinMode(13, OUTPUT);
}

void loop()
{
  
  
  buttonState = digitalRead(2);
  Serial.println(buttonState);
  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState == HIGH) {
    // turn LED on:
    digitalWrite(13, LOW);
  } else {
    // turn LED off:
    digitalWrite(13, HIGH);
  }
  
}