// C++ code
//
int checkButtonHit = 0;

int Display = 0;

int PrevCheckButtonHit = 0;

void setup()
{
  pinMode(3, INPUT);
  Serial.begin(9600);
  pinMode(13, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(7, OUTPUT);

  Display = -1;
}

void loop()
{
  PrevCheckButtonHit = digitalRead(3);
  Serial.println(Display);
  if (PrevCheckButtonHit == 0) {
    checkButtonHit = 0;
  }
  if (checkButtonHit == 0 && PrevCheckButtonHit == 1) {
    Display = (Display + 1);
    checkButtonHit = 1;
  }
  Display = (Display % 3);
  if (Display == 0) {
    digitalWrite(13, HIGH);
    digitalWrite(12, HIGH);
    digitalWrite(11, HIGH);
    digitalWrite(10, HIGH);
    digitalWrite(9, HIGH);
    digitalWrite(8, HIGH);
    digitalWrite(7, LOW);
  }
  if (Display == 1) {
    digitalWrite(13, LOW);
    digitalWrite(12, HIGH);
    digitalWrite(11, HIGH);
    digitalWrite(10, LOW);
    digitalWrite(9, LOW);
    digitalWrite(8, LOW);
    digitalWrite(7, LOW);
  }
  if (Display == 2) {
    digitalWrite(13, HIGH);
    digitalWrite(12, HIGH);
    digitalWrite(11, LOW);
    digitalWrite(10, HIGH);
    digitalWrite(9, HIGH);
    digitalWrite(8, LOW);
    digitalWrite(7, HIGH);
  }
  delay(10); // Delay a little bit to improve simulation performance
}