/*
  First Lab Arduino
*/
int timer = 200;   
int pin = 13;
void setup() {
    pinMode(pin, OUTPUT);
}

void loop() {
    // turn the pin on:
    digitalWrite(pin, HIGH);
    delay(timer);
    
    // turn the pin off:
    digitalWrite(pin, LOW);
    delay(timer);
    
}

