#include <TM1637Display.h>


// Countdown Timer
const unsigned long COUNTDOWN_TIME = 300; // 5 minutes in seconds


// Pins for TM1637 display module
#define CLK_PIN 2
#define DIO_PIN 3


TM1637Display display(CLK_PIN, DIO_PIN);


unsigned long startTime;
unsigned long currentTime;
unsigned long elapsedTime;


void setup() {
  display.setBrightness(7); // Set the brightness of the display (0-7)
  display.clear(); // Clear the display
  startTime = millis(); // Record the starting time
}


void loop() {
  currentTime = millis(); // Get the current time
  elapsedTime = (currentTime - startTime) / 1000; // Calculate elapsed time in seconds


  if (elapsedTime <= COUNTDOWN_TIME) {
    unsigned long remainingTime = COUNTDOWN_TIME - elapsedTime;


    // Display remaining time in Minutes:Seconds format
    unsigned int minutes = remainingTime / 60;
    unsigned int seconds = remainingTime % 60;
    display.showNumberDecEx(minutes * 100 + seconds, 0b01000000, true);


    if (remainingTime == 0) {
      // Start blinking when countdown reaches 00:00
      while (true) {
        display.showNumberDecEx(0, 0b01000000, true); // Display "00:00"
        delay(500);
        display.clear(); // Clear the display
        delay(500);
      }
    }
  }


  delay(1000); // Wait for 1 second
}