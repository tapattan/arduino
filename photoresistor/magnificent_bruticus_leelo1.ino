// C++ code
//

void setup()
{
  pinMode(A0, INPUT);
  Serial.begin(9600);
  pinMode(11, INPUT);
}

void loop()
{
  int p1 = analogRead(A0);
  float m = 255.0/555;
  float k = 255-(m*p1);
  
  Serial.print(p1);
  Serial.print(' ');
  Serial.print(k);
  Serial.println();
  analogWrite(11,k);
  
  delay(10); // Delay a little bit to improve simulation performance
}