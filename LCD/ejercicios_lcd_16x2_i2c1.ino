#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

// address 0x3F 
LiquidCrystal_I2C lcd(0x20,16,2);  

void setup() {
  // Inicializar el LCD
  lcd.init();
  
  lcd.backlight();
  lcd.setCursor(0,1);// col , row
  lcd.print("Hello");
  delay(10);
}

void loop() {
  lcd.setCursor(0,0); // col , row
  lcd.print("Thailand");
  lcd.backlight();
  delay(10);
}