// C++ code
//
int start = 0;

int p1 = 0;

int p2 = 0;

void setup()
{
  pinMode(13, INPUT);
  pinMode(12, INPUT);
  pinMode(11, INPUT);
  pinMode(7, OUTPUT);
  pinMode(6, OUTPUT);
}

void loop()
{
  start = digitalRead(13);
  if (start == 1) {
    p1 = digitalRead(12);
    p2 = digitalRead(11);
    digitalWrite(7, LOW);
    digitalWrite(6, LOW);
  }
  if (p1 == 1) {
    digitalWrite(7, HIGH);
    delay(5000); // Wait for 5000 millisecond(s)
  } else {
    digitalWrite(7, LOW);
    if (p2 == 1) {
      digitalWrite(6, HIGH);
      delay(5000); // Wait for 5000 millisecond(s)
    } else {
      digitalWrite(6, LOW);
    }
  }
}